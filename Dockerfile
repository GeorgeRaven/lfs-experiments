ARG ARCH=arm64
ARG CROSS_COMPILE=aarch64-none-elf-gnu-

FROM alpine:edge as builder
ARG ARCH
ARG CROSS_COMPILE
RUN apk add git && \
    mkdir -p ${HOME}/git/ && \
    cd ${HOME}/git && \
    git clone https://github.com/u-boot/u-boot.git && \
    cd ${HOME}/git/u-boot && \
    git fetch --tags && \
    latestTag=$(git describe --tags `git rev-list --tags --max-count=1`) && \
    git checkout $latestTag && \
    echo $latestTag
WORKDIR /root/git/u-boot
COPY utils utils
# creating the entrypoint file
RUN apk add \
    bash \
    bison \
    build-base \
    flex \
    gawk \
    gcc \
    gcc-cross-embedded \
    make \
    perl \
    python3 \
    texinfo \
    xz
RUN bash utils/version-check.sh && \
    echo "#!/usr/bin/env bash" > entrypoint.sh && \
    echo "ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILE} make qemi_arm64_defconfig" >> entrypoint.sh && \
    echo "ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILE} make -j16" >> entrypoint.sh && \
    chmod 744 entrypoint.sh
#CMD sh build.sh
# CMD /bin/sh
