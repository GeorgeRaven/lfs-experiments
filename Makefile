TAG=lfs/compiler
CONTAINER_NAME=exporter
EXPORT_DIR=public

.PHONY: all
all: build run

.PHONY: build
build:
	docker build -t ${TAG} -f Dockerfile .

.PHONY: run
run:
	xhost local:root
	docker run -e DISPLAY --volume /tmp/.X11-unix:/tmp/.X11-unix -it ${TAG}

.PHONY: cp
cp: build
	docker rm -f ${CONTAINER_NAME}
	docker run -d --name ${CONTAINER_NAME} --entrypoint /bin/sleep ${TAG} 20
	docker cp ${CONTAINER_NAME}:/app/static ${EXPORT_DIR}
	docker rm -f ${CONTAINER_NAME}
